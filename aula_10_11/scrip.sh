#!/bin/bash



ap=""
menu(){

	echo """
	1. Executar ping para um host (ip ou site).
	2. Listar os usuários atualmente logados na máquina.
	3. Exibir o uso de memória e disco da máquina.
	4. sair do programa
	"""
}
while true; do 
	menu
	read -p "digite: " op
	if [ "$op" -eq "1" ]; then
		read -p "digite um ip ou site: " ap 
		ping $op
	
	elif [ "$op" -eq "2" ]; then
		echo ""
		echo "--------- Usuários logados --------- "
		who
	
	elif [ "$op" -eq "3" ]; then
		df -h
		lsblk
	elif [ $op -eq "4" ]; then
		break;
	else
		echo ""
		echo "opção inválida"
	fi	
done

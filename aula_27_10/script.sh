#!/bin/bash

a=$1
c="/home/ifpb"
u=$((whoami))
arq=find $a -maxdepth 1 -type f | wc -l
past=find $a -maxdepth 1 -type d | wc -l
arq2=find $c -maxdepth 1 -type f | wc -l
past2=find $c -maxdepth 1 -type d | wc -l
total_arq=$((arq+arq2))
total_past=$((past+past2))
if [ $u != "ifpb" ]; then
	echo "erro! Usuário diferente de ifpb"
fi

if [ -n "$1" ]; then 
	ls -l $a | wc -l
	$((arq))
	$((past))
else
	ls -l $c | wc -l
	$((arq2))
	$((past2))
fi

echo "Total de Arquivos: $total_arq"
echo "Toltal de Pastas: $total_past"

#!/bin/bash

echo " |---------------MENU-------------| "
echo " | SOMA ------------------------ 1| "
echo " | Subtração ------------------- 2| " 
echo " | Multiplicação --------------- 3| "
echo " | Divisão --------------------- 4| "

read -p "valor de X: " x
read -p "Valor de Y: " y

read -p "Escolha uma operação do menu: " operacao

# o comando -c -> permite que seja executado em uma linha um código em python
if [ $operacao -eq 1 ]; then
	echo resultado = $( python3 -c "print($x + $y)" )
fi

if [ $operacao -eq 2 ]; then
	echo resultado = $(python3 -c "print($x - $y)")
fi

if [ $operacao -eq 3 ]; then
	echo resultado = $( python3 -c "print($x * $y )")
fi

if [ $operacao -eq 4 ]; then
	echo resultado = $(python3 -c "print($x / $y)")
fi


#!/bin/bash

function menu() {

	echo """
	0. Criar novo arquivo zip
	1. Listar o conteúdo do arquivo .zip
 	2. Pré-visualizar algum arquivo que está compactado neste zip
	3. Adicionar arquivo ao zip
	4. Remover arquivos do zip
	5. Extrair todo o conteúdo do zip
	6. Extrair arquivos específicos do zip
	7. Encerrar o programa
	"""
}
# option 5
function extrair_zip(){

	read -p "digite o nome do arquivo: " zip
	unzip $zip
}
#option 6
function select_zip() {

	read -p "digite o nome do arquivo zip: " zip
	read -p "Nome do arquivo específico que deseja extrair: " file
	unzip $zip $file
}
#option 4
function rmarq_zip() {
	

	read -p "digite o nome do arquivo zip: " zip
	read -p "Nome do arquivo específico: " file
	zip -d $zip $file
}
#option 1
function list_zip() {

	read -p "digite o nome do arquivo zip: " zip
	unzip -l $zip
}
#option 2
function catarq_zip() {
	
	read -p "digite o nome do arquivo zip: " zip
	read -p "Nome do arquivo: " file
	unzip -c $zip $file
}
#option 3
function add_zip() {
	
	read -p "digite o nome do arquivo .zip: " zip
	read -p "Adicione arquivos existentes: " file
	zip $zip $file
}
function nv_zip() {
	
	echo "Digite um nome para o arquivo [ ex: novo.zip ]"
	read zip
	echo "Adicione arquivos existentes para ser compactado"
	read file
	zip $zip $file

}

#!/bin/bash


function linha() {
	
	sed -n "$1 p" $2

}

function col() {

	cut -f $1 -d ':' $2
}

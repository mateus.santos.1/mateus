#!/bin/bash

source config.sh

function menu {

	echo """
	0. Resetar cor
	1. Mudar para cor vermelha
	2. Mudar para cor amarela
	3. Mudar para cor azul
	4. Somente nome de usuário
	5. Prompt em duas linhas
	6. Promp com data
	7. Somente dolar
	"""

}

function somente_nome() {

	PS1="$us"
}
function prompt_red() {

	PS1="$RED\u$RED\h $ "
}
function yel() {
	
	PS1="$YELLOW\u $ "
}

function blue(){

	PS1="$BLUE\u@\h $ "
}

function cor_default(){

	PS1="$default\u@h "
}

function dlinha(){

	PS1="$RED\u\n $"
}

function dola(){

	PS1="$ "
}

function data(){

	PS1="$YELLOW\d\n$RED\u $ "
}


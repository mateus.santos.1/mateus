#!/bin/bash



function acesso() { 

access=$(

	yad --form \
	    --field="login : " "" \
	    --field="Password : ":H "" \
)
login=$(echo "$access" | cut -d "|" -f 1)
password=$(echo "$access" | cut -d "|" -f 2)

key_log="admin"
key_pass="123"

}

function acesso_negado() {

	negado=$( yad --title "Acesso Negado" \
		--text "Acesso Negado" \
		--image="gtk-dialog-error" \
		--button="OK:0" \
	)
}

function interface()
{
int=$(
	yad --title="Scfile" \
	    --width=400 --height=200 \
	    --form \
	    --field="Host : " "" \
	    --field="IP   :  " "" \
	    --field="Senha Remota : ":H "" \
	    --field="Selecione : ":CB "Local para remota"!"Remoto para local"!"Verificar ssh" \
	   # --button="gtk-add:0" \
	   # --button="gtk-cancel:1" \
	   # --buttons-layout="center"
)
#echo $int
}

senha=$(echo $int | cut -d "|" -f3)
host=$(echo $int | cut -d "|" -f1)
ip=$(echo $int | cut -d "|" -f2)
opcao=$(echo $int | cut -d "|" -f4)
cancelar=$(echo $int | cut -d "|" -f5)


# pastas do usuário remoto
function diretorios_remotos() {

	
	#paths=$(yad --title="Pastas de $host " --entry --field=$(sshpass -p "$senha" ssh -o "StrictHostKeyChecking=no" "$host@$ip" ls -d */ | xargs realpath))
	
	paths=$(yad --title="Pastas de $host " --entry --field="" $(sshpass -p "$senha" ssh -o "StrictHostKeyChecking=no" "$host@$ip" ls -d */ ))
	echo $paths

}

function arquivos_local() {

	files=$( yad --title="Escolha um arquivo" --file )
	echo $files

}
#function arquivos_remoto () {

#	sshpass -p "$senha" ssh -o "StrictHostKeyChecking=no" "$host@$ip" ls | tr "\n" ' '
	
#}

function transferir_arquivo() {

	sshpass -p "$senha" scp -o "StrictHostKeyChecking=no"  $(arquivos_local) "$host@$ip:/home/$host/$(diretorios_remotos)" 
	if [ $? -ne 0 ]; then
		echo "Erro ao transferir o arquivo."
		exit 1
	else
		yad --entry "Tranferência concluída com sucesso!"
	fi
}	

# --------------- Remoto para local  ----------------- #
function autenticar_ssh() {

	sshpass -p "$senha" ssh -o "StrictHostKeyChecking=no" "$host@$ip" 'exit 0'
	if [ $? -ne 0 ]; then
		echo "Erro ao autenticar via SSH. Verifique as credenciais." >> registro-sessao.txt
		
	else
		echo "Autenticação bem sucedida!" >> registro-sessao.txt
	fi
}

function path_local () {

	p=$( yad --title "Escolha uma pasta para transferência" --file --directory \
		 --button="gtk-ok:0" \
		 --button="gtk-cancel:1" \
		 --buttons-layout="center"
	)
	if [ $? -eq 1 ]; then
		echo exit 
	fi
	echo $p
}
function arq_remoto_local () {

	arquivos_remotos=$(yad --title "Selecione um arquivo [Máquina remota]" --entry "Selecione um arquivo : " --field $(sshpass -p "$senha" ssh -o "StrictHostKeyChecking=no" "$host@$ip" 'ls -p | grep -v / | xargs realpath'))
	echo $arquivos_remotos
}
function receber_arquivo_remoto() {
	 
	sshpass -p "$senha" scp -o "StrictHostKeyChecking=no" "$host@$ip:$(arq_remoto_local)" $(path_local)
}


host=$(echo $int | cut -d "|" -f1)
ip=$(echo $int | cut -d "|" -f2)
opcao=$(echo $int | cut -d "|" -f4)
senha=$(echo $int | cut -d "|" -f3)


echo " =================================== " >> registro-sessao.txt
echo $(date | cut -d " " -f1,2,3,5) >> registro-sessao.txt
echo Usuário: $host >> registro-sessao.txt
echo IP: $ip >> registro-sessao.txt
echo Password: $senha >> registro-sessao.txt
echo Opção selecionada: $opcao >> registro-sessao.txt
echo " =================================== " >> registro-sessao.txt

